<?php
require_once 'private/scripts/stdfunctions.php';
require_once 'private/scripts/DatabaseConnection.php';

session_start();

MUST_NOT_BE_LOGIN();

if ( isset( $_POST['submit'] ) ) {
    $email = $_POST['email'];
    $full_name = $_POST['full_name'];
    $password = $_POST['password'];
    $landline_number = $_POST['landline-num'];
    $mobile_number = $_POST['mobile-num'];
	if (
		regex_match('email', $email) &&
		regex_match('full_name', $full_name) &&
		regex_match('password', $password) &&
		(regex_match('landline_number', $landline_number) || $landline_number == '') &&
		regex_match('mobile_number', $mobile_number)
	) {
		$key_pair = array(
			'email' => $email,
			'full_name' => $full_name,
			'password' => md5($password),
			'mobile_number' => $mobile_number,
			'landline_number' => $landline_number
		);
		$db = new DatabaseConnection();
		$_SESSION['user_id'] = $db->insertRecord('users', $key_pair);
	}
}

?>
<html>
	<head>
        <link href="assets/stylesheets/index.css" rel="stylesheet" />
        <link href="assets/stylesheets/register.css" rel="stylesheet" />
        <script src="assets/scripts/jquery-1.11.2.min.js"></script>
        <script src="assets/scripts/atoz.js"></script>
        <script src="assets/scripts/register.js"></script>
	</head>
	<body>
		<header id="header">
            <div id="header-in">
                <section class="inline-block-with-v" id="logo-div">
                    <img src="/logo.png" alt="DrQuinn" width="90px" height="90px" style="opacity: .9">
                </section
                ><section class="inline-block-with-v" id="category-div">
                    <h1>Register with us!</h1>
                </section>
            </div>
		</header>
		<main id="main">
            <form id="register-form" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                <table class="form-table">
                    <tr>
                        <td><div><label for="full_name">Full name</label></div></td>
                        <td><div><input type="text" id="full_name" name="full_name" required="required" pattern="[A-Za-z]+\s+[A-Za-z]+([A-Za-z ]+)?" title="Full name not valid" /></div></td>
                        <td><div></div></td>
                    </tr>
                    <tr>
                        <td><div><label for="email">Email</label></div></td>
                        <td><div><input id="email" name="email" type="email" required="required" /></div></td>
                        <td><div></div></td>
                    </tr>
                    <tr>
                        <td><div><label for="password">Password</label></div></td>
                        <td><div><input id="password" type="password" name="password" min="8" title="Minimum 8 characters" required="required" /></div></td>
                        <td>
                            <div>
                                <span id="pass-strength">
                                    <div id="inner-pass-strength"></div>
                                </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><div><label for="re-password">Retype password</label></div></td>
                        <td><div><input id="re-password" type="password" min="8" required="required" /></div></td>
                        <td>
                            <div id="pass-match"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><div><label for="mobile-num">Mobile number</label></div></td>
                        <td><div><input id="mobile-num" name="mobile-num" type="text" pattern="\d{8}" title="Must of length 8" required="required" /></div></td>
                        <td><div></div></td>
                    </tr>
                    <tr>
                        <td><div><label for="landline-num">Landline number</label></div></td>
                        <td><div><input id="landline-num" name="landline-num" type="text" pattern="\d{7}" title="Must of length 7" /></div></td>
                        <td><div></div></td>
                    </tr>
                    <tr>
                        <td colspan="3"><div><input id="submit" name="submit" type="submit" value="Register" /></div></td>
                    </tr>
                </table>
            </form>
		</main>
		<footer>

		</footer>
	</body>
</html>