<header id="header">
	<div id="header-in">
		<section class="inline-block-with-v" id="logo-div">
			<img src="/logo.png" alt="DrQuinn" width="90px" height="90px" style="opacity: .9">
		</section
			><section class="inline-block-with-v" id="category-div">
			<div id="search-div">
				<form action="search.php" method="get" id="search-form" >
					<select name="category" id="search-category">
						<option value="0">All category</option>
						<option value="1">Cat 1</option>
						<option value="2">Cat 2</option>
						<option value="3">Cat 3</option>
						<option value="4">Cat 4</option>
						<option value="5">Cat 5</option>
						<option value="6">Cat 6</option>
						<option value="7">Cat 7</option>
						<option value="8">Cat 8</option>
						<option value="9">Cat 9</option>
						<option value="10">Cat 10</option>
					</select
						><input type="text" name="criteria" id="search-criteria" placeholder="Need something? Search here!" <?php echo isset($_GET['criteria']) ? 'value="'.$_GET['criteria'].'"' : '' ?>
						/><input type="submit" id="search-submit" value="Search!"
				/></form>
			</div>
			<ul id="category">
				<li class="inline-block-with-v blue">
					<div>Category one</div>
					<div>
						<ul>
							<li>cat 1 a</li>
							<li>cat 1 b</li>
							<li>cat 1 c</li>
							<li>cat 1 d</li>
						</ul>
					</div>
				</li
					><li class="inline-block-with-v orange">
					<div>Category two</div>
					<div>
						<ul>
							<li>cat 2 a</li>
							<li>cat 2 b</li>
							<li>cat 2 c</li>
							<li>cat 2 d</li>
						</ul>
					</div>
				</li
					><li class="inline-block-with-v green">
					<div>Category three</div>
					<div>
						<ul>
							<li>cat 3 a</li>
							<li>cat 3 b</li>
							<li>cat 3 c</li>
							<li>cat 3 d</li>
						</ul>
					</div>
				</li
					><li class="inline-block-with-v yellow">
					<div>Category four</div>
					<div>
						<ul>
							<li>cat 4 a</li>
							<li>cat 4 b</li>
							<li>cat 4 c</li>
							<li>cat 4 d</li>
						</ul>
					</div>
				</li>
			</ul
				></section
			><section class="inline-block-with-v" id="login-div">
			<div>
                <?php if (is_login()) { ?>
                    <a href="profile.php" style="text-decoration: none;font-size: 1.2em;font-style: italic"><?php echo $user['full_name'] ?><br /></a>
                    <a href="/logout.php?from=<?php echo urlencode( $_SERVER['REQUEST_URI'] ) ?>" style="color: #7d0000">logout</a>
                <?php }else{ ?>
                    <button>Login / Register</button>
                <?php } ?>
			</div>
		</section>
	</div>
</header>
<main id="main">
	<div id="main-in">
