<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/private/scripts/DatabaseConnection.php';

class ActiveRecord {
	private $database_connection;
	private $id;
	protected static $table;
	protected static $class;
	protected static $validations;
	public $validation_errors = array();

	function __construct()
	{
		$this->database_connection = new DatabaseConnection();
		if(func_num_args() == 1)
		{
			$this->assignAttribute(func_get_arg(0));
		}
	}

	public function id()
	{
		return $this->id;
	}

	private function assignAttribute($attr)
	{
		$class_vars = get_class_vars(static::$class);
		foreach($class_vars as $k => $v)
		{
			if($k != 'table' && $k != 'class' && $k != 'validations' && $k != 'database_connection' && $k != 'validation_errors')
			{
				$this->$k = isset($attr[$k]) ? $attr[$k] : null;
			}
		}
	}

	private function attributeToArray()
	{
		$attributeToArray = array();
		$class_vars = get_class_vars(static::$class);
		foreach($class_vars as $k => $v)
		{
			if($k != 'table' && $k != 'class' && $k != 'validations' && $k != 'database_connection' && $k != 'validation_errors')
			{
				if($this->$k != null){
					$attributeToArray[$k] = $this->$k;
				}
			}
		}
		return $attributeToArray;
	}

	private function toUpdateString()
	{
		return DatabaseConnection::toUpdateString(static::$table, $this->id(), $this->attributeToArray());
	}

	private function toInsertString()
	{
		return DatabaseConnection::toInsertString(static::$table, $this->attributeToArray());
	}

	private function validate()
	{
		$errors = array();
		if(static::$validations != null)
		{
			foreach(static::$validations as $key => $validations)
			{
				foreach($validations as $type => $value)
				{
					$type == 'regex' ?
						preg_match($value, $this->$key) ? null
							: $errors[] = isset($validations['message']) ? $validations['message'] : "'$key' did not match regex expression"
						: null;
					$type == 'length' ?
						strlen($this->$key) == $value ? null
							: $errors[] = isset($validations['message']) ? $validations['message'] : "'$key' has invalid length"
						: null;
					$type == 'min_length' ?
						strlen($this->$key) >= $value ? null
							: $errors[] = isset($validations['message']) ? $validations['message'] : "'$key' has invalid length"
						: null;
					$type == 'max_length' ?
						strlen($this->$key) <= $value ? null
							: $errors[] = isset($validations['message']) ? $validations['message'] : "'$key' has invalid length"
						: null;
				}
			}
		}
		$this->validation_errors = $errors;
	}

	public function refresh()
	{
		if(func_num_args() == 0)
		{
			$this->assignAttribute($this->database_connection->findRecord(static::$table, $this->id()));
		}
		elseif(func_num_args() == 1)
		{
			$this->assignAttribute(array(func_get_arg(0) => $this->database_connection->findRecord(static::$table, $this->id())[func_get_arg(0)]));
		}
	}

	public function withId($id)
	{
		$this->id = $id;
		$this->refresh();
	}

	public function getOriginal($key)
	{
		return $this->database_connection->findRecord(static::$table, $this->id())[$key];
	}

	public function setId($id){$this->withId($id);} // Alias to withId($id)

	public function discardChanges()
	{
		$this->refresh();
	}

	public function update($key_value)
	{
		if(func_num_args() == 1)
		{
			$this->database_connection->updateRecord(static::$table, $this->id(), $key_value);
			$this->refresh();
		}
		elseif (func_num_args() == 2)
		{
			$this->database_connection->updateRecord(static::$table, $this->id(), array(func_get_arg(0) => func_get_arg(1)));
			$this->refresh();
		}
	}

	public function save()
	{
		if($this->id() == null)
		{
			$this->validate();
			if($this->validation_errors == null){
				$this->before_create();
				print_r($this->attributeToArray());
				$id = $this->database_connection->insertRecord(static::$table, $this->attributeToArray());
				$this->id = $id;
				$this->after_create();
			}
		}
		else
		{
			$this->validate();
			if($this->validation_errors == null){
				$this->before_update();
				$this->database_connection->updateRecord(static::$table, $this->id(), $this->attributeToArray());
				$this->after_update();
			}
		}
	}

	public static function find($id)
	{
		$instance = new static::$class ();
		$instance->withId($id);
		return $instance;
	}

	public static function create($key_value)
	{
		$instance = new static::$class ($key_value);
		$instance->save();
		return $instance;
	}

	protected function before_create()
	{}

	protected function after_create()
	{}

	protected function before_update()
	{}

	protected function after_update()
	{}

}