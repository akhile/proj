<?php

require_once 'ActiveRecord.php';

class User extends ActiveRecord
{
	protected static $table = "users";
	protected static $class = __CLASS__;
	protected static $validations = array(
		'email' => array('regex' => '/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+$/', 'message' => 'email not in correct format' ),
		'full_name' => array( 'regex' => '/^[A-Za-z]+\s+[A-Za-z]+([A-Za-z ]+)?$/', 'message' => 'full name not in correct format' ),
		'password' => array('min_length' => 8, 'max_length' => 32, 'message' => 'password must be between 8 and 32 characters' ),
		'mobile_number' => array('length' => 8 ),
		'landline_number' => array('length' => 7 ),
		'address' => array('min_length' => 10, 'message' => 'password must be at least 10 characters' )
	);

	// Below are the table column headings definition
	public $email;
	public $full_name;
	public $password;
	public $mobile_number;
	public $landline_number;
	public $address;

	protected function before_create()
	{
		$this->password = md5($this->password);
	}


	protected function before_update()
	{
		if($this->getOriginal('password') != $this->password)
		{
			$this->password = md5($this->password);
		}
	}

}