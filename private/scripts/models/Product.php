<?php

require_once 'ActiveRecord.php';

class Product extends ActiveRecord
{
	protected static $table = "products";
	protected static $class = __CLASS__;

	// Below are the table column headings definition
	public $name;
	public $product_condition;
	public $condition_details;
	public $details;
	public $price;
	public $quantity;
	public $webpage;
	public $product_options;
	public $image_url;
	public $storeid;
}