<?php

class DatabaseConnection
{
	public $connection;
	function __construct()
	{
		$host = "localhost";
		$username = "root";
		$password = "";
		$db = "proj";
		$conn = new mysqli( $host , $username , $password , $db );
		$this->connection = $conn;
	}
	function __destruct()
	{
		$this->connection->close();
	}
	public static function toUpdateString($table, $id, $key_value)
	{
		$parsedSet = "";
		foreach($key_value as $k => $v)
		{
			$parsedSet .= $k."='".mysql_real_escape_string($v)."', ";
		}
		$parsedSet = substr($parsedSet, 0, strlen($parsedSet)-2);
		return "UPDATE $table SET $parsedSet WHERE id=$id";
	}
    public static function toInsertString($table, $key_value)
    {
        $key = "";
        $value = "";
        foreach($key_value as $k => $v)
        {
            if( $k != 'id' ) // Does not allow manual insertion of `id`
            {
                $key .= $k.", ";
                $value .= "'".mysql_real_escape_string($v)."', ";
            }
        }
        $key = substr($key,0,strlen($key)-2);
        $value = substr($value,0,strlen($value)-2);
        return "INSERT INTO $table ($key) VALUES ($value)";
    }
    public static function toSelectString($table, $key_value)
    {
        $parsedSet = "";
        foreach($key_value as $k => $v)
        {
            $parsedSet .= $k."='".mysql_real_escape_string($v)."' AND ";
        }
        $parsedSet = substr($parsedSet, 0, strlen($parsedSet)-4);
        return "SELECT * FROM $table WHERE $parsedSet";
    }
	public function updateRecord($table, $id, $key_value)
	{
		$this->connection->query(self::toUpdateString($table, $id, $key_value));
	}
	public function insertRecord($table, $key_value)
	{
		$this->connection->query(self::toInsertString($table, $key_value));
		return $this->connection->insert_id;
	}
    public function findRecord($table, $id)
    {
        $query = $this->connection->query("SELECT * FROM ".$table." WHERE `id` = '".$id."'");
        $record = $query->fetch_assoc();
        return $record;
    }
    public function selectRecords($table, $key_value)
    {
        $query = $this->connection->query($this->toSelectString($table, $key_value));
        return $query;
    }
	public function rawQuery($query)
	{
		$ret = $this->connection->query($query);
		return $ret;
	}
	public function close()
	{
		$this->connection->close();
	}
	public static function emailMatch($email)
	{
		return preg_match('/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+$/', $email);
	}
}