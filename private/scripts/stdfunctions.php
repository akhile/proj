<?php

require_once 'DatabaseConnection.php';

function regex_match($type, $value)
{
	$types = array(
		'email' => '/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+$/',
		'full_name' => '/^[A-Za-z]+\s+[A-Za-z]+([A-Za-z ]+)?$/',
		'password' => '/^.{8,32}$/',
		'mobile_number' => '/^\d{8}$/',
		'landline_number' => '/^\d{7}$/',
		'address' => '/^.{10,}$/'
	);
	return preg_match($types[$type], $value);
}

function MUST_NOT_BE_LOGIN()
{
    if (isset($_SESSION['user_id'])) header('Location: /');
}

function MUST_BE_LOGIN()
{
    if (!isset($_SESSION['user_id'])) header('Location: /');
}

function is_login() {
    $is_login = false;
    if( isset ( $_SESSION['user_id'] ) ) {
        $is_login = true;
    }
    return $is_login;
}

function parse_user_details() {
    $conn = new DatabaseConnection();
    return $conn->findRecord('users', $_SESSION['user_id']);
}