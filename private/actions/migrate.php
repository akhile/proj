<?php

$sql_file = $_GET['sql_file'];

require_once '../scripts/db-connect.php';

echo "connection ";
if($conn->connect_errno)
{
	echo "error";
}
else
{
	echo "success";
}

$query = $conn->query(file_get_contents("../migrations/${sql_file}"));

$conn->close();