<?php
require_once 'private/scripts/stdfunctions.php';

session_start();
if (is_login()) {
    $user = parse_user_details();
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Buy and sell products online in Mauritius</title>
		<link rel="stylesheet" href="assets/stylesheets/index.css" />
		<script src="assets/scripts/jquery-1.11.2.min.js"></script>
		<script src="assets/scripts/atoz.js"></script>
        <script src="assets/scripts/index.js"></script>
	</head>
	<body>
		<?php require 'private/partials/header.php' ?>
				<h1>Our sponsored products...</h1>
				<section class="inline-block-with-v" id="slideshow">
					<div id="long-div" class="float-parent" style="left: 0">
						<div class="slide float-left">
							<img class="slide-img" src="assets/images/g3%20Cropped.jpg" alt="">
							<article class="item-description">LG G3</article>
						</div>
						<div class="slide float-left">
							<img class="slide-img" src="assets/images/vibez2pro%20Cropped.jpg" alt="">
							<article class="item-description">Lenovo VIBE Z2 Pro</article>
						</div>
						<div class="slide float-left">
							<img class="slide-img" src="assets/images/vibex2%20Cropped.jpg" alt="">
							<article class="item-description">Lenovo VIBE X2</article>
						</div>
						<div class="slide float-left">
							<img class="slide-img" src="assets/images/galaxys5%20Cropped%20(1).jpg" alt="">
							<article class="item-description">Samsung Galaxy S5</article>
						</div>
						<div class="slide float-left">
							<img class="slide-img" src="assets/images/nexus6%20Cropped.jpg" alt="">
							<article class="item-description">Moto Nexus 6<br />Simply the best...</article>
						</div>
					</div>
					<div class="slide-next-prev" id="slide-prev">
						&lt;
					</div>
					<div class="slide-next-prev" id="slide-next">
						&gt;
					</div>
				</section
				><section class="inline-block-with-v" id="slide-adjacent">
					<article class="back-blue">
						<div>
							<div class="inline-block-with-v detail">
								<b>Laptop, Tablet, Stand, and Tablet+ Modes</b><br /><br />Work at a desk in laptop mode, get mobile in tablet mode, and share data and presentations by placing Helix in stand mode—simply dock the tablet backward with the enhanced keyboard. Fold it down onto the base for Tablet+ Mode and get more power, ports, and battery life than a standard tablet.
							</div
							><div class="inline-block-with-v" style="background-image: url('assets/images/thinkPad-helix.jpg'); background-size: 390px 170px">

							</div>
						</div>
					</article>
					<article class="back-green">
						<div>
							<div class="inline-block-with-v detail">
								<b>Razer Blade Parallax</b><br /><br />
								Razer set a goal to design the world’s thinnest gaming laptop, for those that need insanely powerful performance in the thinnest form factor for gaming anywhere, anytime. We set out to do the impossible without compromising performance. The 14-inch Razer Blade gaming laptop is one of the thinnest gaming laptops of its class.
							</div
							><div class="inline-block-with-v" style="background-image: url('assets/images/razer-blade-parallax.jpg'); background-size: 390px 220px; background-position: 0 -10px">

							</div>
						</div>
					</article>
				</section
				><h2>Featured products...</h2
				><section id="below-slide">
					<article class="inline-block-with-v">
						<div class="image"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="iPhone 6 Plus" style="background-image: url('assets/images/iphone6-plus.jpg')" /></div>
						<div class="detail back-red">
							iPhone 6 isn't simply bigger - it's better in every way. Larger, yet dramatically thinner. More powerful, but remarkably power efficient.
						</div>
					</article
					><article class="inline-block-with-v">
						<div class="image"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="" style="background-image: url('assets/images/ray-ban.jpg')" /></div>
						<div class="detail back-green">
							Ray-Ban is the global leader in premium eyewear market and by far the best-selling eyewear brand in the world.
						</div>
					</article
					><article class="inline-block-with-v">
						<div class="image"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="" style="background-image: url('assets/images/IMAC.jpg')" /></div>
						<div class="detail back-yellow">
							iMac features an ultrathin all-in-one design, beautiful widescreen display, the latest processors and graphics, and advanced storage options.
						</div>
					</article
					><article class="inline-block-with-v">
						<div class="image"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="" style="background-image: url('assets/images/beats.jpg')" /></div>
						<div class="detail back-black">
							The Beats Acoustic Engine™ makes your listening experience intimate, personal, and real.
						</div>
					</article
					><article class="inline-block-with-v">
						<div class="image"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="" style="background-image: url('assets/images/linksys.png')" /></div>
						<div class="detail back-blue">
							The Linksys E2500 connects all your computers, tablets, Internet-ready TVs, game consoles and other devices at wireless transfer rates of up to 300 + 300 Mbps for an optimal home network experience.
						</div>
					</article
				></section
				>
		<?php require 'private/partials/footer.php' ?>
	</body>
</html>