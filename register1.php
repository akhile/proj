<?php

require_once 'private/scripts/models/User.php';

if(isset($_POST['submit']))
{
	$email = $_POST['email'];
	$full_name = preg_replace('/\s+/', ' ', $_POST['full_name']); # Converts "Bob   marley" to "Bob marley"
	$password = $_POST['password'];
	$address = $_POST['address'];
	$mobile_number = $_POST['mobile_number'];
	$landline_number = $_POST['landline_number'];
	if(/*
		preg_match('/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+$/', $email) &&
		preg_match('/^[A-Za-z]+\s+[A-Za-z]+([A-Za-z ]+)?$/', $full_name) &&
		preg_match('/^.{8,32}$/', $password) &&
		preg_match('/^.{10,}$/', $address) &&
		preg_match('/^\d{8}$/', $mobile_number) &&
		preg_match('/^\d{7}$/', $landline_number)
	*/ true)
	{
		$new_usr = User::create(
			array(
				'email' => $_POST['email'],
				'full_name' => $_POST['full_name'],
				'password' => $_POST['password'],
				'address' => $_POST['address'],
				'mobile_number' => $_POST['mobile_number'],
				'landline_number' => $_POST['landline_number']
			)
		);
		if($new_usr->validation_errors) print_r($new_usr->validation_errors);
	}
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
	<script src="assets/scripts/atoz.js"></script>
    <script>


        function validate(){
			if(document.getElementById('Password').value == document.getElementById('ConPassword').value)
			{
				
				return true;
			}
	        return false;
        }

    </script>

    <style>

        .data{
            height: 80px;
        }

        .error{
            color: red;
            font-size: 12px;
            font-style: italic;
        }


    </style>
</head>

<body>

<form method="post" onsubmit="return validate()" action="<?php echo $_SERVER['PHP_SELF'] ?>">

    <fieldset>
        <legend><b>Login</b></legend>

        <div class="data">
            <label for="Fullname" class="left">Full name: </label>
	        <input name="full_name" pattern="[A-Za-z]+\s+[A-Za-z]+([A-Za-z ]+)?" title="Enter a valid full name" class="input" type="text" id="Fullname" required="required" placeholder="Full name" size="15"/>
            <p class="error" id="ErrFullname"></p>
        </div>

        <div class="data">
            <label for="email" class="left">Email: </label>
	        <input type="email" name="email" id="email" required="required" placeholder="Email Address"/>
            <p class="error" id="Erremail"></p>
        </div>

	    <div class="data">
		    <label for="address" class="left">Address: </label>
		    <textarea name="address" id="address" required="required" placeholder="Address">
		    </textarea>
		    <p class="error" id="ErrAddress"></p>
	    </div>

	    <div class="data">
		    <label for="mobile_number" class="left">Mobile number: </label>
		    <input name="mobile_number" pattern="\d{8}" title="Must be an 8 digit number" type="text" id="mobile_number" required="required" placeholder="Mobile number"/>
	    </div>

	    <div class="data">
		    <label for="landline_number" class="left">Landline number: </label>
		    <input name="landline_number" pattern="\d{7}" title="Must be a 7 digit number" type="text" id="landline_number" required="required" placeholder="Landline number"/>
	    </div>

        <div class="data">
            <label for="Password"  class="left">Password: </label>
	        <input min="8" max="32" title="Should be between 8 to 32 characters" name="password" type="password" id="Password" required="required" placeholder="Password"/>
            <p class="error" id="ErrPassword"></p>
        </div>

        <div class="data">
            <label for="ConPassword" class="left">Confirm Password: </label>
	        <input min="8" max="32" title="Should be between 8 to 32 characters" type="password" id="ConPassword" required="required" placeholder="Confirm Password"/>
            <p class="error" id="ErrConPassword"></p>
        </div>

        <div class="data">
            <input name="submit" class="left" type="submit" value="Submit"/>
        </div>

    </fieldset>

</form>

</body>

</html>