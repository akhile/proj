<?php
require_once 'private/scripts/stdfunctions.php';

if ( !isset($_GET['criteria']) || $_GET['criteria']=='' ) {
    header('Location: /');
}

session_start();
if (is_login()) {
    $user = parse_user_details();
}

$original_criteria = $_GET['criteria'];

$criteria = '%'.implode('%', explode(' ', $original_criteria)).'%';
$criteria = mysql_real_escape_string($criteria);

$conn = new DatabaseConnection();
$qry_string = "SELECT * FROM products WHERE (name LIKE '$criteria' OR details LIKE '$criteria')";
if ( isset($_GET['condition']) ) {
    $condition = mysql_real_escape_string($_GET['condition']);
    $qry_string .= " AND product_condition='$condition'";
}
if ( isset($_GET['price_low']) && isset($_GET['price_high']) ) {
    $low = mysql_real_escape_string($_GET['price_low']);
    $high = mysql_real_escape_string($_GET['price_high']);
    $qry_string .= " AND (price BETWEEN $low and $high)";
}
$qry = $conn->rawQuery($qry_string);

?>
<!DOCTYPE html>
<html>
<head>
    <title>Buy and sell products online in Mauritius</title>
    <link rel="stylesheet" href="assets/stylesheets/index.css" />
    <link rel="stylesheet" href="assets/stylesheets/search.css" />
    <script src="assets/scripts/jquery-1.11.2.min.js"></script>
    <script src="assets/scripts/atoz.js"></script>
    <script src="assets/scripts/search.js"></script>
</head>
<body>
    <?php require 'private/partials/header.php' ?>
    <h1>Search results for: "<?php echo $original_criteria ?>"</h1>
    <div class="inline-block-with-v" id="filters">
        <div id="filter-text">Filter result</div>
        <div class="filter-heading">Price:</div>
        <div>
            <input type="number" id="start-price" value="<?php echo isset($_GET['price_low']) ? $_GET['price_low'] : null ?>" style="width: 75px" />
            to
            <input type="number" id="end-price" value="<?php echo isset($_GET['price_high']) ? $_GET['price_high'] : null ?>" style="width: 75px" />
            <input type="submit" value="go" id="submit-price" style="padding: 2px 2px">
        </div>
        <div>
            <input type="submit" value="reset" id="reset-price" style="padding: 2px 5px">
        </div>
        <div class="filter-heading">Condition:</div>
        <div>
            <input value="a" <?php echo !isset($_GET['condition']) || $_GET['condition'] == '' ? 'checked="checked"' : '' ?> type="radio" name="condition" id="any-cond" /><label for="any-cond"> any</label>
        </div>
        <div>
            <input value="n" <?php echo isset($_GET['condition']) && $_GET['condition'] == 'n' ? 'checked="checked"' : '' ?> type="radio" name="condition" id="new-cond" /><label for="new-cond"> new</label>
        </div>
        <div>
            <input value="r" <?php echo isset($_GET['condition']) && $_GET['condition'] == 'r' ? 'checked="checked"' : '' ?> type="radio" name="condition" id="refurbished-cond" /><label for="refurbished-cond"> refurbished</label>
        </div>
        <div>
            <input value="u" <?php echo isset($_GET['condition']) && $_GET['condition'] == 'u' ? 'checked="checked"' : '' ?> type="radio" name="condition" id="used-cond" /><label for="used-cond"> used</label>
        </div>
    </div
    ><div class="inline-block-with-v" id="search-container">
        <?php
        while($row = $qry->fetch_assoc()) {
            $image_conn = new DatabaseConnection();
            $image_qry = $image_conn->selectRecords('images', array('product_id' => $row['id']));
            $images = array();
            $images_description = array();
            while ( $image_row = $image_qry->fetch_assoc ( ) ) {
                $images[] = '/assets/images/' . $image_row['id'] . '.' . $image_row['extension'];
                $images_description[] = $image_row['description'];
            }
            ?><div class="search-result" data-href="/product.php?id=<?php echo $row['id'] ?>">
                <div class="float-left" style="width: 151px; height: 150px">
                    <img class="result-product-image" alt="<?php echo $images_description[0] ?>" src="<?php echo $images[0] ?>" width="150px" height="150px" style="border-right: solid 1px rgba(0,0,0,.5)" data-array="<?php echo implode(',', $images) ?>" data-current="0" />
                </div>
                <div class="float-left" style="width: 199px; height: 150px; position: relative">
                    <div style="overflow: hidden; max-height: 126px">
                        <div style="padding: 5px 10px; font-size: 1.2em; font-weight: 900">
                            <a class="product-name" href="/product.php?id=<?php echo $row['id'] ?>"><?php echo $row['name'] ?></a>
                        </div>
                        <div style="padding: 5px 10px; font-style: italic"><?php echo $row['details'] ?></div>
                    </div>
                    <div style="position: absolute; bottom: 0; left: 0; right: 0; text-align: right; padding: 5px 10px; color: #CC1111">
                        Rs. <?php echo $row['price'] ?>
                    </div>
                </div>
            </div><?php
        }
        ?>
    </div>
    <?php require 'private/partials/footer.php' ?>
</body>
</html>