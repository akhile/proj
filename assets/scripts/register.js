function passStrength(){
    var val = document.getElementById('password').value;
    var strength = 0;
    var passStrength = document.getElementById('inner-pass-strength');
    if(/[ \`\~\!\@\#\$\%\^\&\*\(\)\_\+\=\-\{\}\]\[\\\|\:\;\"\'\<\>\?\/\,\.]/.test(val)) {
        strength++;
    }
    if(val.length >= 8) {
        strength++;
    }
    if(/[A-Z]/.test(val)) {
        strength++;
    }
    if(/\d/.test(val)) {
        strength++;
    }
    if (strength == 4) {
        passStrength.className = "four"
    } else if(strength == 3) {
        passStrength.className = "three"
    } else if(strength == 2) {
        passStrength.className = "two"
    } else if(strength == 1) {
        passStrength.className = "one"
    } else {
        passStrength.className = ""
    }
}
function passMatch(){
    var val = document.getElementById('re-password').value;
    var passMatch = document.getElementById('pass-match');
    if(val == '') { passMatch.innerHTML = '';return; }
    if(val == document.getElementById('password').value) {
        passMatch.innerHTML = '&check;';
        passMatch.style.color = 'green';
    } else {
        passMatch.innerHTML = '&cross;';
        passMatch.style.color = 'red';
    }
}
window.addEventListener('load', function(){
    document.getElementById('password').addEventListener('keyup', function(){passStrength();passMatch()});
    document.getElementById('re-password').addEventListener('keyup', function(){passMatch();passStrength()})
});