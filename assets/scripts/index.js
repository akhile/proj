$longDiv = null;
slideTimeout = null;
slidePrev = function() {
    var left = parseInt($longDiv.style.left);
    var opacity = parseInt($longDiv.style.opacity);
    if(left == -600 || left == -1200 || left == -1800 || left == -2400) {
        var slideNum = left / -600;
        O_o.animate({elem: $longDiv, property: 'left', to: slideNum*-600 + 600 + 'px', duration: 1500, callback: function(){
            clearTimeout(slideTimeout);
            slideTimeout = setTimeout(slideNext, 5000)
        }, type: O_o.effect.fastSlow})
    }else if(left == 0 && opacity == 1) {
        O_o.animate({elem: $longDiv, property: 'opacity', from: 1, to: '0', duration: 750, callback: function(){
            $longDiv.style.left = '-2400px';
            O_o.animate({elem: $longDiv, property: 'opacity', to: '1', duration: 750, callback: function(){
                clearTimeout(slideTimeout);
                slideTimeout = setTimeout(slideNext, 5000)
            }})
        }})
    }
};
slideNext = function(){
    var left = parseInt($longDiv.style.left);
    var opacity = parseInt($longDiv.style.opacity);
    if(left == 0 || left == -600 || left == -1200 || left == -1800) {
        var slideNum = left / -600;
        O_o.animate({elem: $longDiv, property: 'left', to: slideNum*-600 - 600 + 'px', duration: 1500, callback: function(){
            clearTimeout(slideTimeout);
            slideTimeout = setTimeout(slideNext, 5000)
        }, type: O_o.effect.fastSlow})
    }else if(left == -2400 && opacity == 1) {
        O_o.animate({elem: $longDiv, property: 'opacity', from: 1, to: '0', duration: 750, callback: function(){
            $longDiv.style.left = 0;
            O_o.animate({elem: $longDiv, property: 'opacity', to: '1', duration: 750, callback: function(){
                clearTimeout(slideTimeout);
                slideTimeout = setTimeout(slideNext, 5000)
            }})
        }})
    }
};


window.addEventListener('load', function() {
    $longDiv = document.getElementById("long-div");
    $longDiv.style.opacity = 1;
    $slidePrev = document.getElementById("slide-prev");
    $slideNext = document.getElementById("slide-next");
    slideTimeout = setTimeout(slideNext, 5000);
    $slidePrev.addEventListener('click', slidePrev);
    $slideNext.addEventListener('click', slideNext)
});
