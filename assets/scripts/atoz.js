overlay = {
	display: function(title, content){
		document.querySelector("#overlay > div #overlay-title").innerHTML = title;
		document.querySelector("#overlay > div #overlay-content").innerHTML = content;
		document.getElementById("overlay").style.display = "block";
		var margin = (window.innerHeight - document.querySelector("#overlay > div").clientHeight) / 2;
		document.querySelector("#overlay > div").style.margin = margin + "px auto";
	},
	close: function(){
		document.getElementById("overlay").style.display = "none";
	}
};
loginFormHtml = '';
O_o = {
	animate: function (args){ // *elem, *property, from, *to, duration, type
		var elem = args.elem;
		var css_property = args.property.split('-');
		var js_property = css_property[0];
		for(var i = 1;i < css_property.length; i++){
			js_property += css_property[i].replace(/^[a-z]/, function(c){return c.toUpperCase()})
		}
		var unit = args.to.replace(/^(-\d+|\d+)/, "");
		var from = parseFloat(args.from || eval("elem.style." + js_property) || 0);
		var to = parseFloat(args.to);
		var difference = to - from;
		var startTime = new Date();
		var id = setInterval(function(){
			var timeElapsed = new Date() - startTime;
			var progress = timeElapsed / (args.duration || 1000);
			if (progress > 1) progress = 1;
			var delta = (args.type && args.type(progress)) || O_o.effect.linear(progress);
			var new_val = delta*difference + from;
			eval("elem.style." + js_property + " = '" + new_val + unit + "'");
			if (progress == 1){
				args.callback && args.callback();
				clearInterval(id)
			}
		}, 20)
	},
	effect: {
		slowFast: function(progression){
			return Math.sin(progression*(Math.PI/2) + (Math.PI*3/2)) + 1
		},
		fastSlow: function(progression){
			return Math.abs(Math.sin(progression*(Math.PI/2)))
		},
		slowFast2: function(progression){
			return Math.pow(progression, 2)
		},
		linear: function(progression){
			return progression
		},
		slowFastSlow: function(progression){
			return (Math.sin(progression*(Math.PI) + (Math.PI*3/2)) + 1)/2
		},
		bounce: function(progression){
			var ret;
			if(progression >=0 && progression < 0.25){
				ret = 1+Math.cos(2*Math.PI*progression - Math.PI);
			}else if(progression >= 0.25 && progression < 0.75){
				ret =  1+(1/2)*Math.cos(2*Math.PI*progression);
			}else if(progression >= 0.75 && progression <= 1){
				ret = 1+(1/4)*Math.cos(4*Math.PI*progression - Math.PI/2);
			}
			return ret;
		}
	}
};
var originalBodyClientHeight;
var adjustHeight = function () {
    if ( originalBodyClientHeight < window.innerHeight ) {
        var $fillElem = document.getElementById('main');
        var deltaHeight = window.innerHeight - document.body.clientHeight;
        var newHeight = deltaHeight + $fillElem.clientHeight;
        $fillElem.style.height = newHeight + 'px';
    }
};

parseUrlParamsToArray = function () {
    var tmpArray = window.location.search.substr(1, window.location.search.length).split('&');
    var array = [];
    for(var j = 0; j < tmpArray.length; j++) {
        array[j] = tmpArray[j].split('=');
    }
    return array;
};

window.addEventListener('load', function(){
    loginFormHtml = document.getElementById('login-content').innerHTML;
    document.getElementById("overlay-close").addEventListener("click", overlay.close);
    $data_href = document.querySelectorAll('[data-href]');
    for(var i = 0; i < $data_href.length; i++) {
        var self = $data_href[i];
        self.addEventListener('click', function(){
            window.location = this.getAttribute('data-href');
        });
    }
    document.querySelector("#login-div > div > button") && document.querySelector("#login-div > div > button").addEventListener("click", function (event) {
        overlay.display("Login now", loginFormHtml);
    });

    // Adjust footer size :D
    originalBodyClientHeight = document.body.clientHeight;
    adjustHeight();
    window.addEventListener('resize', adjustHeight);

    /*
     $('#search-criteria').focus(function(event){
     $('#search-submit').addClass('search-focus');
     $('#search-category').addClass('search-focus');
     });
     $('#search-criteria').blur(function(event){
     $('#search-submit').removeClass('search-focus');
     $('#search-category').removeClass('search-focus');
     });
     */
});
