var imageChangeInterval = 1500;

var timeout;
var nextResultImage = function(self) {
    clearTimeout(timeout);
    var array = self.getAttribute('data-array').split(',');
    (array.length == 1) || O_o.animate({elem: self, property: 'opacity', to: '0', from: '1', duration: 250, callback: function() {
        self.src = array[(parseInt(self.getAttribute('data-current')) + 1) % array.length];
        self.setAttribute('data-current', (parseInt(self.getAttribute('data-current')) + 1) % array.length);
        O_o.animate({elem: self, property: 'opacity', to: '1', from: '0', duration: 250});
    }});
    timeout = window.setTimeout(function(){nextResultImage(self)}, 500 + imageChangeInterval)
};
window.addEventListener('load', function() {
    var $results = document.getElementsByClassName('search-result');
    for ( var i = 0; i < $results.length; i++ ) {
        $results[i].addEventListener('mouseenter', function() {
            var image = this.children[0].children[0];
            timeout = window.setTimeout(function() {
                nextResultImage(image);
            }, imageChangeInterval);
        });
        $results[i].addEventListener('mouseleave', function() {
            window.clearTimeout(timeout);
        });
    }
    $condition = document.getElementsByName('condition');
    for( var int = 0; int < $condition.length; int++) {
        $condition[int].addEventListener('click', function() {
            var arr = parseUrlParamsToArray();
            var is_found = false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i][0] == 'condition') {
                    if(this.value != 'a')
                        arr[i][1] = this.value;
                    else
                        arr.splice(i, 1);
                    is_found = true;
                }
            }
            if (!is_found)
                if(this.value != 'a')
                    arr[arr.length] = ['condition', this.value];
            window.location.search = '?' + arr.join('&').split(',').join('=');
        });
    }
    document.getElementById('submit-price').addEventListener('click', function (event) {
        var low = parseFloat(document.getElementById('start-price').value);
        var high = parseFloat(document.getElementById('end-price').value);
        if(!(low && high && high > low)) return;
        console.log('ok');
        var is_found = false;
        var arr = parseUrlParamsToArray();
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][0] == 'price_low') {
                is_found = true;
                arr[i][1] = low;
            }
            if (arr[i][0] == 'price_high') {
                arr[i][1] = high;
            }
        }
        if(!is_found) {
            arr[arr.length] = ['price_low', low];
            arr[arr.length] = ['price_high', high];
        }
        window.location.search = '?' + arr.join('&').split(',').join('=');
    });
    document.getElementById('reset-price').addEventListener('click', function (event) {
        var arr = parseUrlParamsToArray();
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][0] == 'price_low') {
                arr.splice(i, 1);
            }
            if (arr[i][0] == 'price_high') {
                arr.splice(i, 1);
            }
        }
        window.location.search = '?' + arr.join('&').split(',').join('=');
    })
});