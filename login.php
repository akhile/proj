<?php

require_once 'private/scripts/DatabaseConnection.php';

if ( isset ( $_POST[ 'submit' ] ) ) {
    $from = isset($_GET['from']) ? $_GET['from'] : '/';
    $kvp = array( 'email' => $_POST['email'], 'password' => md5($_POST['password']) );
    $conn = new DatabaseConnection();
    $query = $conn->selectRecords('users', $kvp);
    if ( $query->num_rows == 1 ) {
        $assoc = $query->fetch_assoc();
        session_start();
        $_SESSION['user_id'] = $assoc['id'];
        header('Location: '.$from);
    }
}

?>